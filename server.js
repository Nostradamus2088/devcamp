const path = require('path');
const express = require ('express');                                        /* const logger = require ('./middleware/logger'); */
const dotenv = require ('dotenv');
const morgan = require('morgan');
const colors = require('colors');
const fileupload = require('express-fileupload');
const cookieParser = require('cookie-parser');
const errorHandler = require('./middleware/error')
const connectDB = require('./config/db');


//load env vars
dotenv.config({ path: './config/config.env'});


//connect to database
connectDB();

//Route files
const bootcamps = require('./routes/bootcamps');
const courses = require('./routes/courses');
const auth = require('./routes/auth');
const users = require('./routes/users');
const reviews = require('./routes/reviews');

const app = express();

//body parser
app.use(express.json());

// Cookie parser
app.use(cookieParser());

//dev logging middleware
if (process.env.NODE_ENV === 'development')
{
    app.use(morgan('dev'));
}

//file upload
app.use(fileupload());

//set statis folder
app.use(express.static(path.join(__dirname, 'public')));

                                                                                            /* app.use(logger); */

//Mount routers
app.use('/api/v1/bootcamps', bootcamps);
app.use('/api/v1/courses', courses);
app.use('/api/v1/auth', auth);
app.use('/api/v1/users', users);
app.use('/api/v1/reviews', reviews);


//put middleware after the mount or it wont work cause its used in a lineair way
app.use(errorHandler);
                                                                    
const PORT = process.env.PORT || 5000;

 const server = app.listen(
    PORT, 
    console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`.yellow.bold)
    );


//handle unhandled promise rejections
process.on('unhandledRejection',(err, promise)=>{
    console.log(`Error: ${err.message}`.red);
    //close server and exit process
    server.close(()=> process.exit(1)); //exit with failure 
});














    /* res.send({name: 'brad'}); */ // send A json object use res.json is better for json objects
                                                                        /* res.json({name: 'brad'}); */ //send json file as content
                                                                        /* res.sendStatus(400);  */        //send status exemple u want to send the status
                                                                        /* res.status(200).json({success: true, data: {id:1}}); */ //sending the status true with the data 