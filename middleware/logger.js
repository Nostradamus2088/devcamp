// @desc  logs request to console
const logger = (req,res, next) => {  
    /*  req.hello = 'hello world';  *///set a value on req object for then have access in any route
  
     console.log(`${req.method} ${req.protocol}://${req.get('host')}${req.originalUrl}`);  //     :// give u acces to the host and url
     next(); //pass to the next middleware in line
  };

  module.exports = logger;  //access to it on all files